import { Injectable } from '@angular/core';

import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseAuthState  } from 'angularfire2';

import { tokenNotExpired } from 'angular2-jwt';


import { authConfig } from "../config";

//import {ROUTER_DIRECTIVES, Router, Location} from "@angular2/router";
import {Routes} from "@angular/router";
import {Router, RouterModule} from '@angular/router';

// Avoid name not found warnings
declare var Auth0Lock: any;

@Injectable()
export class AuthService {

  loggedIn: boolean = false;
  userName: string = "UNKNOWN";


  
  authOptions:{} = {
    languageDictionary: {
      title: "WayCare"
    },
    theme: {
      logo: '../images/LogoFace.png',
      primaryColor: '#124259' // match the primary color of WayCare logo
    }  
  };

  lock = new Auth0Lock(authConfig.clientID, authConfig.domain, this.authOptions);


  
  constructor(public af: AngularFire, private router:Router) {

    // Add callback for lock `authenticated` event
    this.lock.on('authenticated', (authResult) => {
      localStorage.setItem('id_token', authResult.idToken);
    });
  
  }


  public login() {
    // Call the show method to display the widget.
    this.lock.show();
  };

  public authenticated() {
    // Check if there's an unexpired JWT
    // It searches for an item in localStorage with key == 'id_token'
    //console.log("auth:", tokenNotExpired())
    return tokenNotExpired();
  };

  public logout() {
    // Remove token from localStorage
    localStorage.removeItem('id_token');

     //switch to login-screen
     this.router.navigate(['/']);

  };




}
