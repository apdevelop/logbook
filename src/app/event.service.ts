import { Injectable} from '@angular/core';

import {AngularFire, FirebaseListObservable, FirebaseObjectObservable, FirebaseAuthState} from "angularfire2";
import {AuthService} from "./auth/auth.service";

import { QuestionService } from './dynamic-form/question.service';

import { appDefaults } from "./config";

import { MdlDialogService } from "angular2-mdl";


@Injectable() export class EventService {


  private currentType: string;



  private latestId: number[] = [];
  private latestSectionEntryIndex: number[] = [];


  private eventUnderEdit;

  dbItemsForDisplay: FirebaseListObservable<any[]>[] = [];  // used for what we actually display


  constructor(private qs: QuestionService, private af: AngularFire, private authService: AuthService,
      private dialogService: MdlDialogService) {
  

     this.currentType = appDefaults['startType'];

     this.initDbItems();


  }




  initDbItems() {


    this.qs.getTypes().forEach(type => 
      { 
        this.dbItemsForDisplay[type] = this.af.database.list(this.getDbPath(type), {
          query: {
              //orderBy: 'title',
              //equalTo: "io1",
          }
        } );
      }
      );

  }




  getDbPath(type) {

    return ( type.toLowerCase() + "s" );

  }




  // add the event in  places
  // We first write to:
  //       incidents/<incident-json>
  // and then for each affected section we set 
  //       sections/<section>/incidents/<incident-json>
  addEvent(event: {}) {


    let type = event['type'];

    let newEventRef = this.af.database.list(this.getDbPath(type)).push(event);
    //console.log("newRef.name=", newEventRef.key);

    // now add also to each affected section
    let sections = event['affectedSections'].replace(/\s+/g, '').split(",");
    for (var i = 0, len = sections.length; i < len; i++) {

      if (sections[i] == "") continue;

      let eventRef = {valid : true};
      this.af.database.object("sections/"  + sections[i] + "/" + 
        this.getDbPath(type) + "/" + newEventRef.key).set(eventRef); 

    }

  }

  
  deleteEvent(event: {}) {

    // Need to delete it's reference in all sections and then to delete the event itself
    // Enh: try to do both as one atomic operation

    console.log("in event.service deleteEvent", event['$key']);

    let key = event['$key'];
    if (! key) {
      let result = this.dialogService.alert("Can't delete w/o valid $key");
      //result.subscribe( () => console.log('alert closed') );
      return;
    }

    let type = event['type'];
    if (! type) {
      let result = this.dialogService.alert("Can't delete w/o valid type attribute");
      //result.subscribe( () => console.log('alert closed') ); 
      return;
    }


    // delete ref in each affected section
    let sections = event['affectedSections'].replace(/\s+/g, '').split(",");
    for (var i = 0, len = sections.length; i < len; i++) {

      if (sections[i] == "") continue;

      let ref = this.af.database.object("sections/"  + sections[i] + "/" + 
        this.getDbPath(type) + "/" + key); 

      ref.remove();
    }

    // delete the event itself
    let ref = this.af.database.object(this.getDbPath(type) + "/" + key);

    ref.remove();

  }
  


  // update same entry in the db 
  // 1. override the event in it's $key location
  // 2. update aaffected sections (some may need to be updated)
  updateEvent(key, event, oldAffectedSections) {

    // 1. override the event in it's $key location


    console.log("in event.service updateEvent", event['$key']);

    if (! key) {
      let result = this.dialogService.alert("Can't update w/o valid $key");
      return;
    }

    let type = event['type'];
    if (! type) {
      let result = this.dialogService.alert("Can't update w/o valid type attribute");
      return;
    }

    // update the event itself
    this.af.database.object(this.getDbPath(type) + "/" + key).set(event); 




    // 2. update aaffected sections (some may need to be updated)

    // in order to trigger refresh on platform, we prefer to delete & insert the references in the sections

    // delete ref in each affected section
    //let sections = event['affectedSections'].replace(/\s+/g, '').split(",");
    let sections = oldAffectedSections.replace(/\s+/g, '').split(",");
    for (var i = 0, len = sections.length; i < len; i++) {

      if (sections[i] == "") continue;

      let ref = this.af.database.object("sections/"  + sections[i] + "/" + 
        this.getDbPath(type) + "/" + key); 

      ref.remove();
    }

    // insert for each section
    sections = event['affectedSections'].replace(/\s+/g, '').split(",");
    for (var i = 0, len = sections.length; i < len; i++) {

      if (sections[i] == "") continue;

      let eventRef = {valid : true};
      this.af.database.object("sections/"  + sections[i] + "/" + 
        this.getDbPath(type) + "/" + key).set(eventRef); 

    }


  }


  setCurrentType(type) {
    this.currentType = type;
  }

  getCurrentType() {
    return this.currentType;
  }




  setEventUnderEdit(event) {
    this.eventUnderEdit = event;
  }

  getEventUnderEdit() {
    return this.eventUnderEdit;
  }


  clearEventUnderEdit() {
    this.eventUnderEdit = null;
  }



}