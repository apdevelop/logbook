export class QuestionBase<T>{
  value: T;
  key: string;
  label: string;
  required: boolean;
  useKeyAsValue: boolean;
  order: number;
  hideCond: string;
  controlType: string;
  constructor(options: {
      value?: T,
      key?: string,
      label?: string,
      required?: boolean,
      useKeyAsValue?: boolean,
      order?: number,
      hideCond?: string,
      controlType?: string
    } = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.hideCond = options.hideCond || '';
    this.required = !!options.required;
    this.useKeyAsValue = !!options.useKeyAsValue;
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
  }
}
