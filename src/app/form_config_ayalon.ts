


export const formConfig_ayalon = {
	
  Common: {




    titleQ: {
      type: 'Textbox',
      cfg: {
        key: 'title',
        label: 'Title',
        value: '',
        required: true,  
        order: 10
      }
    },
    



    LatLongitudeQ: {
      type: 'Textbox',
      cfg: {
        key: 'latLongitude',
        label: 'Latitude,Longitude',
        type: 'text',    
        value: '',
        required: true,  
        order: 30
      }
    },



    classQcheckBox: {
      type: 'MultiCheckbox',
      cfg: {
        key: 'affectedSections',
        label: 'Affected sections',
        useKeyAsValue: true,
        options: [
          {key: '0',  text: 'Ha Shirion Jct - Arlozorov Jct (North Bound)', value: false},
          {key: '1',  text: 'Arlozorov Jct - Glilot Jct (North Bound)', value: false},
          {key: '2',  text: 'Rehavam Zeevi Br - Arlozorov Br (South Bound)', value: false},
          {key: '3',  text: 'Arlozorov Br - Holon Jct (South Bound)', value: false},
        ],
        order: 90
      }
    },


  },




  Incident: {




    timeCbQ: {
      type: 'Checkbox',
      cfg: {
        key: 'currentTime',
        text: 'Current time',
        label: 'Time', 
        value: true, // checked
        order: 20
      }
    },


    dateTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'dateTime',
        type: 'datetime-local',
        label: 'Date-Time',
        value: '',
        required: false,  
        hideCond: 'currentTime',     // hidden when currentTime is checked  
        order: 21
      }
    },



    iconTypeQ: {
      type: 'Radio',
      cfg: {
        key: 'icon',
        label: 'Icon type',
        useKeyAsValue: true,
        value: 'construction', // checked key
        options: [
          {key: 'construction',  value: 'Construction'},
          {key: 'crash-major',  value: 'Crash-major'},
          {key: 'crash-minor',  value: 'Crash-minor'},
          {key: 'incident',  value: 'Incident'},
        ],
        order: 30
      }
    },

  

    incidentTypeQ: { 
       type: 'Dropdown',
       cfg: { 
            key: 'incidentType',
            label: 'Incident type',
            value: 'construction', 
            options: [
              {key: 'construction',  value: 'Construction'},
              {key: 'crash',  value: 'Crash'},
              {key: 'road-incident',  value: 'Road-incident'},
              {key: 'other',  value: 'Other'},
            ],
            order: 50
          }
        },




  },




  Event: {


    startTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'startTime1',
        type: 'datetime-local',
        label: 'Start-Time',
        value: '',
        required: false,  
        order: 20
      }
    },


    endTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'endTime1',
        type: 'datetime-local',
        label: 'End-Time',
        value: '',
        required: false,  
        order: 21
      }
    },



    descQ: {
      type: 'Textbox',
      cfg: {
        key: 'description',
        label: 'Description',
        value: '',
        required: false,  
        order: 40
      }
    },
    


    iconTypeQ: {
      type: 'Radio',
      cfg: {
        key: 'icon',
        label: 'Icon type',
        useKeyAsValue: true,
        value: 'concert', // checked key
        options: [
          {key: 'concert',  value: 'Concert'},
          {key: 'conference',  value: 'Conference'},
          {key: 'crowd',  value: 'Crowd'},
          {key: 'show',  value: 'Show'},
          {key: 'stadium',  value: 'Stadium'},
        ],
        order: 50
      }
    },

  



  },




};


