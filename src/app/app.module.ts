import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from "@angular/router";


// Global components:
import {AppComponent} from './app.component';
import {AuthComponent} from './auth/auth.component';

// Auth
import {AUTH_PROVIDERS} from 'angular2-jwt';
import { AuthGuard } from "./auth/auth.guard";

// Firebase
import { AngularFireModule } from 'angularfire2'; 

// Material design components
import {MdlModule} from 'angular2-mdl';
import {MdlSelectModule} from "@angular2-mdl-ext/select";
import {MdlPopoverModule} from "@angular2-mdl-ext/popover";

// user components
import {HeaderComponent} from './header/header.component';

// services
import {AuthService} from "./auth/auth.service";
import {firebaseConfig} from "./config";

import { HomeComponent } from './home/home.component'; 
import { EventService } from "./event.service";
import { routing } from './app.routing';
import { QuestionControlService } from "./dynamic-form/question-control.service";
import { QuestionService } from "./dynamic-form/question.service";
import { DynamicFormQuestionComponent } from './dynamic-form/dynamic-form-question.component';
import { CardComponent } from './card/card.component';
import { AddComponent } from './add/add.component';
import { ShowComponent } from './show/show.component';
import { ReversePipe } from './show/reverse.pipe';






@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AuthComponent,
    DynamicFormQuestionComponent,
    CardComponent,
    AddComponent,
    ShowComponent,
    ReversePipe,
    

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MdlModule,
    MdlSelectModule,
    MdlPopoverModule,
    AngularFireModule.initializeApp(firebaseConfig),
    HttpModule,
    routing,
   


  ],
  providers: [
    AUTH_PROVIDERS,
    AuthService,
    AuthGuard,
    EventService, QuestionControlService, QuestionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
