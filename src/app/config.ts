/**
 * Created by idanhahn on 11/11/2016.
 */

interface AuthConfiguration{
  clientID: string,
  domain: string
}

export const authConfig: AuthConfiguration = {

   // Logbook client
   clientID: 'KFpWAwfhtbM3sdcN4wRs0qa3L2Njg7Tl', 
   domain: 'waycare-general.auth0.com'

}



// Firebase DataBase configuration
const firebaseConfig_channel10 = {
  apiKey: "AIzaSyCGwFKlWthiWBDuk6U7zZDzoLrDX-ko0Yo",
  authDomain: "channel10-9d152.firebaseapp.com",
  databaseURL: "https://channel10-9d152.firebaseio.com",
  storageBucket: "channel10-9d152.appspot.com",
  messagingSenderId: "287523635077"

};


const firebaseConfig_fll = {
  apiKey: "AIzaSyBiQCA3tkiCTmKHEORQehTVPvCc1oMUj3s",
  authDomain: "fortll-2da0f.firebaseapp.com",
  databaseURL: "https://fortll-2da0f.firebaseio.com",
  storageBucket: "fortll-2da0f.appspot.com",
  messagingSenderId: "178533358063" 
};


const firebaseConfig_las_vegas = {
  apiKey: "AIzaSyCyp1ZigLd0YoZE_7cV67ds4D0KCcD5Jzk",
  authDomain: "las-vegas-a67c8.firebaseapp.com",
  databaseURL: "https://las-vegas-a67c8.firebaseio.com",
  storageBucket: "las-vegas-a67c8.appspot.com",
  messagingSenderId: "448835329274"
};



const firebaseConfig_tampa = {
  apiKey: "AIzaSyAG996tEbBVOC3Xc1EwUAM2ZheHQsv59LA",
  authDomain: "tampa-85515.firebaseapp.com",
  databaseURL: "https://tampa-85515.firebaseio.com",
  storageBucket: "tampa-85515.appspot.com",
  messagingSenderId: "465192946136"
};


// dev
const firebaseConfig_dev = {

    // dev-test
    apiKey: "AIzaSyB0ePipSA6oPpyoSKQShOKIpWwB4a2BPfY",
    authDomain: "platformdev1-e40d7.firebaseapp.com",
    databaseURL: "https://platformdev1-e40d7.firebaseio.com",
    storageBucket: "platformdev1-e40d7.appspot.com",
    messagingSenderId: "258875038188"
 };


// =====>>> Select here <======
export const firebaseConfig = firebaseConfig_channel10;



// formConfig
import { formConfig_ayalon } from './form_config_ayalon';
import { formConfig_fll } from './form_config_fll';
import { formConfig_las_vegas } from './form_config_las_vegas';
import { formConfig_tampa } from './form_config_tampa';

// =====>>> Select here <======
export const formConfig = formConfig_ayalon;


export const appDefaults = {
	startType: 'Incident',
	numEntriesToDisplay: 5,   
};


// Additional configuration here

interface GenericConfiguration{
}

export const config: GenericConfiguration = {
}
