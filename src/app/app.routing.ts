import {Routes} from "@angular/router";
import { RouterModule} from '@angular/router';

import { AppComponent} from "./app.component";
import { HomeComponent} from "./home/home.component";
import { ShowComponent} from './show/show.component';
import { AddComponent} from './add/add.component';

import { AuthGuard } from "./auth/auth.guard";
 


const APP_ROUTES: Routes  = [
		{path:'', component: HomeComponent},   
		{path:'show', component: ShowComponent, canActivate: [AuthGuard]},   
		{path:'add', component: AddComponent, canActivate: [AuthGuard]},   
		{path:'edit', component: AddComponent, canActivate: [AuthGuard]},   
	
];

export const routing = RouterModule.forRoot(APP_ROUTES);
