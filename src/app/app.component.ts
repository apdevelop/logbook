import { Component, ViewContainerRef } from '@angular/core';
import { MdlDialogOutletService} from "angular2-mdl";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

   constructor(
    private dilalogOuletService: MdlDialogOutletService, 
    private viewConatinerRef: ViewContainerRef) {
    this.dilalogOuletService.setDefaultViewContainerRef(this.viewConatinerRef);
  }


}