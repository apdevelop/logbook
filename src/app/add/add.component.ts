import { Component, OnInit} from '@angular/core';
import { FormGroup }                 from '@angular/forms';
import { QuestionBase }              from '../dynamic-form/question-base';
import { QuestionControlService }    from '../dynamic-form/question-control.service';
import { QuestionService } from '../dynamic-form/question.service';
 

import { EventService } from "../event.service";
import {Observable} from 'rxjs/Rx';
import { MdlDialogService } from "angular2-mdl";
import {Router} from '@angular/router';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  isEditMode: boolean = false;

  questions: QuestionBase<any>[] = [];

  form: FormGroup;

  type: string = ''; 
 
  eventTypes = []; 

  readyForEdit: boolean;

  copyMode: boolean;

  questionsCfg;

  eventKey; // the key we need to keep using

  oldAffectedSections = [];

		
  constructor(private qs: QuestionService, private qcs: QuestionControlService, 
    private els: EventService, private dialogService: MdlDialogService,
    private router:Router) { 

   }
  



  typeChanged() {
    //console.log('typeChanged to ' + this.type);
    this.els.setCurrentType(this.type); // remember for next time

    this.questions = this.qs.getQuestions(this.type); 

    this.form = this.qcs.toFormGroup(this.questions);    

  }

  
  ngOnInit() {

  	// are we in 'edit' mode or 'add' 
  	this.isEditMode = (this.els.getEventUnderEdit() != null);


  	this.readyForEdit = false;

  	this.questionsCfg = null;




    if (this.isEditMode) { //--- Edit mode

	    let event = this.els.getEventUnderEdit();
	    this.els.clearEventUnderEdit();

	    if (! event ) {
	    	//alert("no event under edit found");
	    	return;
	    }

	    this.questionsCfg = event['eventEditCfg'];


	    if (! this.questionsCfg ) {
	    	this.dialogService.alert("Missing eventEditCfg property. can't edit this event.");
	    	return;
	    }


	    this.eventKey = event['$key'];

	    if (! this.eventKey ) {
	    	this.dialogService.alert("Missing eventKey property. can't edit this event");
	    	return;
	    }

      this.type = event['type'];

      if (! this.type ) {
        this.dialogService.alert("Missing type property. can't edit this event");
        return;
      }

      this.copyMode = event['__isCopy'];
      event['__isCopy'] = null;

      if (this.copyMode) {
        if (this.questionsCfg['Common']['titleQ'])
          this.questionsCfg['Common']['titleQ']['cfg']['value'] += "(copy)";
      }

	    this.oldAffectedSections = event['affectedSections'];

      this.questions = this.qs.getQuestionsForCfg(this.type, this.questionsCfg); 

	  	this.readyForEdit = true;

  	} else {  //--- Add mode

      this.type = this.els.getCurrentType(); 

      this.eventTypes = this.qs.getTypes(); 

  		this.questions = this.qs.getQuestions(this.type); 

  	}



    this.form = this.qcs.toFormGroup(this.questions);  


  }




  onSubmit() {

    console.log('at EventListAddFormComponent: onSubmit; type=', this.type);

    // debug...
    //console.log("form.value=", this.form.value);
    //console.log(this.form); 

    let eventData = this.qs.getFormAnswers(this.type, this.form.value);




    //--- replace latLongtitde with two entries
    let latLongAry = eventData['latLongitude'].split(",");
    
    // check it first
    
    if  ( (latLongAry.length !=2) || isNaN(latLongAry[0])  || isNaN(latLongAry[1]) ) {
      this.dialogService.alert('Latitude,Longitude format must be: number,number');
      return;
    }

    eventData['lat'] = parseFloat(latLongAry[0]);
    eventData['lng'] = parseFloat(latLongAry[1]);

    delete eventData['latLongitude'];


    eventData['type'] = this.type;


    //--- Incident specific
    if (this.type == 'Incident') {   


      let date: Date;
      if (! this.form.value.currentTime) {
        date = new Date(eventData['dateTime'] .replace(/T/, ' ')); 
      } else {
        date = new Date();  
      }

      // fix the time 
      eventData['time'] = date.toString();
      delete eventData['dateTime'];
      delete eventData['currentTime'];

      eventData['hour'] = date.toTimeString().substring(0,5);


      // ensure we use previous time on edit
      var moment = require('moment');
      this.form.value.currentTime = false;
      this.form.value.dateTime = moment(date).format().substring(0,16);


    } 




    //--- Event specific
    if (this.type == 'Event') {   
      // 
      eventData['startTime'] = eventData['startTime1'].substring(11);
      eventData['endTime'] = eventData['endTime1'].substring(11);
    } 


    //console.log("uploading eventData:", eventData);

    eventData['eventEditCfg'] = this.qs.getFilledQuestionsCfg(this.type, this.form.value);


    // final steps
    if (this.isEditMode) { // Edit/Copy

      // insert it to the DB
      if (this.copyMode) {
        this.els.addEvent(eventData); // Like Add
      } else {
      	this.els.updateEvent(this.eventKey, eventData, this.oldAffectedSections);

      }

    	this.readyForEdit = null; // will clear the page by *ngIf  
   
   		this.router.navigate(['/show']);
   	}
   	else { // Add 

	    // insert it to the DB
	    this.els.addEvent(eventData);


	    // avoid re-submit - clear the form
	    this.questions = this.qs.getQuestions(this.type); 
	    this.form = this.qcs.toFormGroup(this.questions);     
  
   	}


  }



  formReady() {
    // either user left currentTime checked or filled the date 
    return (
        this.form.controls['currentTime'].value || 
        (this.form.controls['dateTime'].value != "")
        );

  }



  onCancel() {

    console.log('at onCancel');
    if (this.isEditMode) {

      this.readyForEdit = null; // will clear the page by *ngIf  
   
      this.router.navigate(['/show']);

    } else {
    console.log('at onCancel-1');

      this.questions = this.qs.getQuestions(this.type); 
      this.form = this.qcs.toFormGroup(this.questions);  
      this.router.navigate(['/show']);

    }


  }
    
  
}
