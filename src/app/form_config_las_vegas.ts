


export const formConfig_las_vegas = {
	
  Common: {




    titleQ: {
      type: 'Textbox',
      cfg: {
        key: 'title',
        label: 'Title',
        value: '',
        required: true,  
        order: 10
      }
    },
    



    LatLongitudeQ: {
      type: 'Textbox',
      cfg: {
        key: 'latLongitude',
        label: 'Latitude,Longitude',
        type: 'text',    
        value: '',
        required: true,  
        order: 30
      }
    },



    classQcheckBox: {
      type: 'MultiCheckbox',
      cfg: {
        key: 'affectedSections',
        label: 'Affected sections',
        useKeyAsValue: true,
        options: [
          {key: '0', text: 'West Desert Inn Rd.', value: false},
          {key: '1', text: 'East Desert Inn Rd.- West of Eastern Av.', value: false},
          {key: '2', text: 'East Desert Inn Rd. - East of Eastern Av.', value: false},
          {key: '3', text: 'I15 - int. 35-42a', value: false},
          {key: '4', text: 'I15 - int. 33-35', value: false},
          {key: '5', text: 'West Flamingo Rd.', value: false},
          {key: '6', text: 'East Flamingo Rd.- East of Eastern Av.', value: false},
          {key: '7', text: 'East Flamingo Rd.- West of Eastern Av.', value: false},
          {key: '8', text: 'North Las Vegas Blv.', value: false},
          {key: '9', text: 'South Las-Vegas Blvd.- North of Sands Av.', value: false},
          {key: '10', text: 'South Las-Vegas Blvd.- South of Sands Av.', value: false},
          {key: '11', text: 'Paradise Rd.- North of Tropicana Av.', value: false},
          {key: '12', text: 'Paradise Rd.- South of Tropicana Av.', value: false},
          {key: '13', text: 'West Tropicana Av.', value: false},
          {key: '14', text: 'East Tropicana Av.- West of Eastern Av.', value: false},
          {key: '15', text: 'East Tropicana Av.- East of Eastern Av.', value: false},
          {key: '16', text: 'West Sahara Av.', value: false},
          {key: '17', text: 'East Sahara Av.- West of Eastern Av.', value: false},
          {key: '18', text: 'East Sahara Av.- East of Eastern Av.', value: false},
          {key: '19', text: 'Spring Montain Rd.', value: false},
          {key: '20', text: 'West Harmon Av.', value: false},
          {key: '21', text: 'East Harmon Av.- West of Eastern Av.', value: false},
          {key: '22', text: 'East Harmon Av.- East of Eastern Av.', value: false},
          {key: '23', text: 'North Maryland Parkway', value: false},
          {key: '24', text: 'South Maryland Parkway', value: false},
        ],
        order: 90
      }
    },


  },




  Incident: {




    timeCbQ: {
      type: 'Checkbox',
      cfg: {
        key: 'currentTime',
        text: 'Current time',
        label: 'Time', 
        value: true, // checked
        order: 20
      }
    },


    dateTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'dateTime',
        type: 'datetime-local',
        label: 'Date-Time',
        value: '',
        required: false,  
        hideCond: 'currentTime',     // hidden when currentTime is checked  
        order: 21
      }
    },



    iconTypeQ: {
      type: 'Radio',
      cfg: {
        key: 'icon',
        label: 'Icon type',
        useKeyAsValue: true,
        value: 'construction', // checked key
        options: [
          {key: 'construction',  value: 'Construction'},
          {key: 'crash-major',  value: 'Crash-major'},
          {key: 'crash-minor',  value: 'Crash-minor'},
          {key: 'incident',  value: 'Incident'},
        ],
        order: 30
      }
    },

  

    incidentTypeQ: { 
       type: 'Dropdown',
       cfg: { 
            key: 'incidentType',
            label: 'Incident type',
            value: 'construction', 
            options: [
              {key: 'construction',  value: 'Construction'},
              {key: 'crash',  value: 'Crash'},
              {key: 'road-incident',  value: 'Road-incident'},
              {key: 'other',  value: 'Other'},
            ],
            order: 50
          }
        },




  },




  Event: {


    startTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'startTime1',
        type: 'datetime-local',
        label: 'Start-Time',
        value: '',
        required: false,  
        order: 20
      }
    },


    endTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'endTime1',
        type: 'datetime-local',
        label: 'End-Time',
        value: '',
        required: false,  
        order: 21
      }
    },



    descQ: {
      type: 'Textbox',
      cfg: {
        key: 'description',
        label: 'Description',
        value: '',
        required: false,  
        order: 40
      }
    },
    


    iconTypeQ: {
      type: 'Radio',
      cfg: {
        key: 'icon',
        label: 'Icon type',
        useKeyAsValue: true,
        value: 'concert', // checked key
        options: [
          {key: 'concert',  value: 'Concert'},
          {key: 'conference',  value: 'Conference'},
          {key: 'crowd',  value: 'Crowd'},
          {key: 'show',  value: 'Show'},
          {key: 'stadium',  value: 'Stadium'},
        ],
        order: 50
      }
    },

  



  },




};


