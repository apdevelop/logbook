import { Component, OnInit} from '@angular/core';
import { EventService } from "../event.service";
import { QuestionService } from '../dynamic-form/question.service';
import { appDefaults } from "../config";



@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {
  
  eventTypes = []; 

  tabTypes = [];

  type: string;

  activeTabIndex = 0;

  filterBy: string = "";

  showLength: number = 0;
  showLimit: number = 5;

  constructor(private qs: QuestionService, private els: EventService) { 

        this.showLimit = appDefaults['numEntriesToDisplay'];
  }


  ngOnInit() {
   
    this.eventTypes = this.qs.getTypes();

    this.tabTypes = this.eventTypes;

    this.type = this.els.getCurrentType();

    this.activeTabIndex = this.tabTypes.indexOf(this.type);


    //console.log("in ngOnInit ", this.tabTypes, this.type , this.activeTabIndex);

  }




  tabChanged(tabChangedEvent) {
    this.els.setCurrentType(this.tabTypes[tabChangedEvent.index]);
    //console.log("tabChangedEvent=", tabChangedEvent);

  } 

  printEventInfo(event) {
    return JSON.stringify(event);
  }


  // return the values stored inside the event (w/o keys) to enable search
  getPureValues(event) {
    var res = "";
    for (var key in event){
       if (key == "eventEditCfg") continue;
       if (event[key] && (event[key] != "")) {
         res += " "  + key + ":"  + event[key];
       }
    }
    return res;
  }


  filter(event, index) {


    if (index == 0) {
      this.showLength = 0; // reset
    }

    if (this.showLength == this.showLimit) {
      return false;  // we are done
    }

    let passFilter = false;

    if (this.filterBy == "") {
      passFilter = true; // faster - no need to check reexp
    } else {
      var re = new RegExp(this.filterBy, 'i');
      passFilter = (this.getPureValues(event).match(re) != null);
    }

    if (passFilter) {
      this.showLength++;
      return true;
    }

    return false;

  }


  onKey(value) {
    // get rid of extra space after ':'
    let value2 = value.replace(/: /, ":");

    this.filterBy = value2;
  }


  setLimit(value) {
        console.log("setLimit=", value, "showLimit=", this.showLimit);
  }


}
