import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {

  transform(arr) {

  	// need to handle null case in order to support obvervables 
  	if (arr != null) {
  		var copy = arr.slice();
    	return copy.reverse();
    } else {
    	return [];
    }

  }

}
